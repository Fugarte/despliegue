<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Inserta los datos para crear un piso</title>

  <!-- Custom fonts for this template-->

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <form action="conexiones/insertarPiso.php" method="post" class="user" enctype="multipart/form-data">
                <div class="form-group row">
				  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="titulo" name="titulo" placeholder="Titulo:" required>
                  </div>
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="int" class="form-control form-control-user" id="n_habitaciones" name="n_habitaciones" placeholder="Numero de Habitaciones:" required>
                  </div>
				   </div>
				  <div class="form-group row">
				  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="int" class="form-control form-control-user" id="precio" name="precio" placeholder="Precio:" required>
					</div>
				  </div>
				<div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="descripcion" name="descripcion" placeholder="Descripcion:" required>
                  </div>
                  <div class="col-sm-6">
                    <input type="int" class="form-control form-control-user" id="distancia" name="distancia" placeholder="Distancia:" required>
                  </div>
                </div>
                <div class="form-group">
                  <input type="int" class="form-control form-control-user" id="telefono" name="telefono" placeholder="Telefono:" minlength="9" maxlength="9" required>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <h4>Selecciona una imagen:</h4>
                    <input type="file" id="imagen" name="imagen" placeholder="Imagen:" required>
                  </div>
				  </div>
				<button type="submit" name="submit" class="btn btn-primary btn-user btn-block">
                  Añadir Piso
                </button>
                <hr>
               
              </form>
              <hr>
              
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

</body>

</html>


