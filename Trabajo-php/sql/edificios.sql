-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-12-2020 a las 23:51:27
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pisos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edificios`
--

CREATE TABLE `edificios` (
  `id` int(11) NOT NULL,
  `Titulo` varchar(50) CHARACTER SET utf16 COLLATE utf16_spanish2_ci NOT NULL,
  `N_Habitaciones` int(2) NOT NULL,
  `Precio` int(6) NOT NULL,
  `Descripcion` varchar(250) CHARACTER SET utf16 COLLATE utf16_spanish2_ci NOT NULL,
  `Distancia` int(8) NOT NULL,
  `Telefono` int(9) NOT NULL,
  `Imagen` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `edificios`
--

INSERT INTO `edificios` (`id`, `Titulo`, `N_Habitaciones`, `Precio`, `Descripcion`, `Distancia`, `Telefono`, `Imagen`) VALUES
(25, 'Piso2', 7, 2000, 'AOSNASODASODASND', 22, 987654321, ''),
(26, 'Piso1', 3, 100, 'AOSNASODASODASND', 21, 987654321, ''),
(27, 'Piso3', 5, 231, 'adssdfdssdf', 31, 345127893, ''),
(28, 'Piso4', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(29, 'Piso5', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(30, 'Piso6', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(31, 'Piso7', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(32, 'Piso8', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(33, 'Piso9', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(34, 'Piso10', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(35, 'Piso11', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(36, 'Piso12', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(37, 'Piso13', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(38, 'Piso14', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(39, 'Piso15', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(40, 'Piso16', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(41, 'Piso17', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(42, 'Piso18', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(43, 'Piso19', 3, 235, 'adssdfdssdf', 78, 345127893, ''),
(44, 'Piso20', 3, 235, 'adssdfdssdf', 78, 345127893, '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `edificios`
--
ALTER TABLE `edificios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `edificios`
--
ALTER TABLE `edificios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
