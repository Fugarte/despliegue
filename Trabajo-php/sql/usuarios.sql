-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-12-2020 a las 23:51:34
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `usuarios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id` int(1) NOT NULL,
  `Nick` varchar(50) CHARACTER SET utf16 COLLATE utf16_spanish2_ci NOT NULL,
  `Nombre` varchar(25) CHARACTER SET utf16 COLLATE utf16_spanish2_ci NOT NULL,
  `Apellido` varchar(25) CHARACTER SET utf16 COLLATE utf16_spanish2_ci NOT NULL,
  `Correo` varchar(25) CHARACTER SET utf16 COLLATE utf16_spanish2_ci NOT NULL,
  `Contrasena` varchar(25) CHARACTER SET utf16 COLLATE utf16_spanish2_ci NOT NULL,
  `Edad` int(11) NOT NULL,
  `Telefono` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id`, `Nick`, `Nombre`, `Apellido`, `Correo`, `Contrasena`, `Edad`, `Telefono`) VALUES
(1, 'Pepe', 'Pepe', 'Garcia', 'pepe@gmail.com', 'pepe', 32, 978654312);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` int(2) NOT NULL,
  `Usuario` varchar(100) CHARACTER SET utf16 COLLATE utf16_spanish_ci NOT NULL,
  `MUsuario` varchar(250) CHARACTER SET utf16 COLLATE utf16_spanish_ci NOT NULL,
  `MAdministrador` varchar(250) CHARACTER SET utf16 COLLATE utf16_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(1) NOT NULL,
  `Nick` varchar(50) CHARACTER SET utf16 COLLATE utf16_spanish2_ci NOT NULL,
  `Nombre` varchar(25) CHARACTER SET utf16 COLLATE utf16_spanish2_ci NOT NULL,
  `Apellido` varchar(25) CHARACTER SET utf16 COLLATE utf16_spanish2_ci NOT NULL,
  `Correo` varchar(25) CHARACTER SET utf16 COLLATE utf16_spanish2_ci NOT NULL,
  `Contrasena` varchar(25) CHARACTER SET utf16 COLLATE utf16_spanish2_ci NOT NULL,
  `Edad` int(11) NOT NULL,
  `Telefono` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `Nick`, `Nombre`, `Apellido`, `Correo`, `Contrasena`, `Edad`, `Telefono`) VALUES
(8, 'Juan', 'Juanito', 'Garces', 'juan@gmail.com', 'pepe', 22, 123456789),
(11, 'Fran', 'Francisco', 'Ugarte García', 'fugarte121@gmail.com', '1', 12, 615566127);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
